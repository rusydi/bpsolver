#include<algorithm>
#include<cassert>
#include<fstream>
#include<iostream>
#include<iterator>
#include<vector>

#include"binary_puzzle.hpp"

bool is_solved;

inline bool c1_violated(const signed char x, const signed char y, const signed char z)
{
    return ((x + y + z != -3) && x == y && y == z);
}

inline bool c1_row_violated(const binary_puzzle & puzzle, size_t i, size_t j)
{
    const signed char &x = puzzle[i][j];
    const signed char &y = puzzle[i][j+1];
    const signed char &z = puzzle[i][j+2];

    return c1_violated(x, y, z);
}

inline bool c1_col_violated(const binary_puzzle & puzzle, size_t i, size_t j)
{
    const signed char &x = puzzle[i][j];
    const signed char &y = puzzle[i+1][j];
    const signed char &z = puzzle[i+2][j];

    return c1_violated(x, y, z);
}

inline void c1_row_fill(binary_puzzle & puzzle, size_t i, size_t j, bool & updated)
{
    const signed char &x = puzzle[i][j];
    const signed char &y = puzzle[i][j+1];
    const signed char &z = puzzle[i][j+2];

    if ((x == -1) && (y != -1) && (y == z))
    {
        puzzle.set_value(i, j, y == 0);
        updated = true;
    }
    else if ((y == -1) && (x != -1) && (x == z))
    {
        puzzle.set_value(i, j+1, x == 0);
        updated = true;
    }
    else if ((z == -1) && (x != -1) && (x == y))
    {
        puzzle.set_value(i, j+2, x == 0);
        updated = true;
    }
}

inline void c1_col_fill(binary_puzzle & puzzle, size_t i, size_t j, bool & updated)
{
    const signed char &x = puzzle[i][j];
    const signed char &y = puzzle[i+1][j];
    const signed char &z = puzzle[i+2][j];

    if ((x == -1) && (y != -1) && (y == z))
    {
        puzzle.set_value(i, j, y == 0);
        updated = true;
    }
    else if ((y == -1) && (x != -1) && (x == z))
    {
        puzzle.set_value(i+1, j, x == 0);
        updated = true;
    }
    else if ((z == -1) && (x != -1) && (x == y))
    {
        puzzle.set_value(i+2, j, x == 0);
        updated = true;
    }
}

inline bool c2_row_violated(const binary_puzzle & puzzle, size_t i)
{
    return (puzzle.is_row_filled(i) &&
            (puzzle.row_nzeros(i) != puzzle.row_nones(i)));
}

inline bool c2_col_violated(const binary_puzzle & puzzle, size_t i)
{
    return (puzzle.is_col_filled(i) &&
            (puzzle.col_nzeros(i) != puzzle.col_nones(i)));
}

inline void c2_row_fill(binary_puzzle & puzzle, size_t index, bool & updated)
{
    size_t halfsize = puzzle.size() >> 1;

    if (puzzle.row_nzeros(index) == halfsize ||
        puzzle.row_nones(index) == halfsize)
    {
        signed char val = puzzle.row_nzeros(index) == halfsize;
        for(size_t j = 0; j < puzzle.size(); ++j)
        {
            if (puzzle[index][j] == -1)
                puzzle.set_value(index, j, val);
        }
        updated = true;
    }
}

inline void c2_col_fill(binary_puzzle & puzzle, size_t index, bool & updated)
{
    size_t halfsize = puzzle.size() >> 1;

    if (puzzle.col_nzeros(index) == halfsize ||
        puzzle.col_nones(index) == halfsize)
    {
        signed char val = puzzle.col_nzeros(index) == halfsize;
        for(size_t i = 0; i < puzzle.size(); ++i)
        {
            if (puzzle[i][index] == -1)
                puzzle.set_value(i, index, val);
        }
        updated = true;
    }
}

inline bool c3_row_violated(const binary_puzzle & puzzle, size_t x, size_t y)
{
    return std::equal(puzzle[x].begin(), puzzle[x].end(), puzzle[y].begin());
}

inline bool c3_col_violated(const binary_puzzle & puzzle, size_t x, size_t y)
{
    for(size_t i = 0; i < puzzle.size(); ++i)
        if (puzzle[i][x] != puzzle[i][y])
            return false;

    return true;
}

/*
bool c3_row_fill(binary_puzzle & puzzle, size_t x, size_t y)
{
}

bool c3_col_fill(binary_puzzle & puzzle, size_t x, size_t y)
{
}
*/

void constraint_propagation(binary_puzzle & puzzle)
{
    while(true)
    {
        bool updated = false;

        /* propagation of constraint 1 */
        for(size_t i = 0; i < puzzle.size(); ++i)
            for(size_t j = 0; j < puzzle.size(); ++j)
            {
                //c1 rowcheck
                if (j <= puzzle.size() - 3)
                {
                    if (c1_row_violated(puzzle, i, j))
                    {
                        puzzle.set_validity(false);
                        return;
                    }
                    else
                        c1_row_fill(puzzle, i, j, updated);
                }

                //c1 colcheck
                if (i <= puzzle.size() - 3)
                {
                    if (c1_col_violated(puzzle, i, j))
                    {
                        puzzle.set_validity(false);
                        return;
                    }
                    else
                        c1_col_fill(puzzle, i, j, updated);
                }
            }

        /* propagation of constraint 2 */
        for(size_t i = 0; i < puzzle.size(); ++i)
        {
            if (puzzle.is_row_balanced(i))
                continue;

            if (c2_row_violated(puzzle, i))
            {
                puzzle.set_validity(false);
                return;
            }
            else
                c2_row_fill(puzzle, i, updated);
        }

        for(size_t i = 0; i < puzzle.size(); ++i)
        {
            if (puzzle.is_col_balanced(i))
                continue;

            if (c2_col_violated(puzzle, i))
            {
                puzzle.set_validity(false);
                return;
            }
            else
                c2_col_fill(puzzle, i, updated);
        }

        /* propagation of constraint 3 */
        for(size_t i = 0; i < puzzle.size() - 1; ++i)
        {
            if (puzzle.is_row_filled(i))
            {
                for(size_t j = i + 1; j < puzzle.size(); ++j)
                {
                    if (!puzzle.is_row_filled(j))
                        continue;
                    if (c3_row_violated(puzzle, i, j))
                    {
                        puzzle.set_validity(false);
                        return;
                    }
                }
            }

            if (puzzle.is_col_filled(i))
            {
                for(size_t j = i + 1; j < puzzle.size(); ++j)
                {
                    if (!puzzle.is_col_filled(j))
                        continue;
                    if (c3_col_violated(puzzle, i, j))
                    {
                        puzzle.set_validity(false);
                        return;
                    }
                }
            }
        }

        if (!updated)
            break;
    }
}

void solve(binary_puzzle puzzle, size_t x, size_t y, signed char val)
{
    //if (is_solved)
    //    return;

    puzzle.set_value(x, y, val);
    constraint_propagation(puzzle);

    if (!puzzle.is_valid())
        return;
    else
    {
        if (!puzzle.has_empty_cell())
        {
            std::cout << "Solution :" << std::endl;
            std::cout << puzzle << std::endl << std::endl;
            is_solved = true;
            return;
        }
        else
        {
            std::pair<size_t, size_t> p = puzzle.get_empty_cell();
            solve(puzzle, p.first, p.second, 0);
            solve(puzzle, p.first, p.second, 1);
        }
    }
}

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        std::cerr << "usage : " << argv[0] << " <input file>" << std::endl;
        return 1;
    }

    is_solved = false;
    binary_puzzle puzzle(argv[1]);

    constraint_propagation(puzzle);

    if (puzzle.is_valid())
    {
        if (!puzzle.has_empty_cell())
            std::cout << puzzle << std::endl;
        else
        {
            std::pair<size_t, size_t> p = puzzle.get_empty_cell();
            solve(puzzle, p.first, p.second, 0);
            solve(puzzle, p.first, p.second, 1);
        }
    }
    else
    {
        if (!puzzle.has_empty_cell())
            std::cerr << "puzzle has no solution" << std::endl;
    }

    return 0;
}
