#ifndef BINARY_PUZZLE_HPP
#define BINARY_PUZZLE_HPP

#include<boost/functional/hash.hpp>
#include<fstream>
#include<iomanip>
#include<stdexcept>
#include<string>
#include<unordered_set>
#include<vector>

class binary_puzzle
{
private:
    std::vector<std::vector<signed char>> _M;
    bool _is_valid;
    size_t _size;
    std::vector<std::size_t> _row_nzeros, _row_nones;
    std::vector<std::size_t> _col_nzeros, _col_nones;
    std::unordered_set<std::pair<size_t, size_t>,
                       boost::hash<std::pair<size_t, size_t>>> _empty_cells;

    void _inc_counters(size_t x, size_t y)
    {
        if (_M[x][y] == 0)
        {
            ++_row_nzeros[x];
            ++_col_nzeros[y];
        }
        else if (_M[x][y] == 1)
        {
            ++_row_nones[x];
            ++_col_nones[y];
        }
    }

    signed char _fmtconverter(const char c)
    {
        switch(c)
        {
        case '*':
            return -1;
            break;
        case '0':
            return 0;
            break;
        case '1':
            return 1;
            break;
        default:
            throw std::runtime_error("invalid character present in input file");
            break;
        };
    }

public:
    binary_puzzle() : _is_valid(true), _size(0) {};

    binary_puzzle(const char * filepath) : _is_valid(true), _size(0)
    {
        init(filepath);
    }

    binary_puzzle(const std::string & filepath) : _is_valid(true), _size(0)
    {
        init(filepath.c_str());
    }

    void init(const char * filepath)
    {
        std::ifstream puzzlefile;
        puzzlefile.open(filepath);

        if (!puzzlefile.is_open())
        {
            throw std::runtime_error("Error opening file");
        }

        std::string line;

        std::getline(puzzlefile, line);
        _size = line.size();
        _M.resize(_size);

        _row_nzeros.resize(_size);
        _row_nones.resize(_size);
        _col_nzeros.resize(_size);
        _col_nones.resize(_size);

        std::fill(_row_nzeros.begin(), _row_nzeros.end(), 0);
        std::fill(_row_nones.begin(), _row_nones.end(), 0);
        std::fill(_col_nzeros.begin(), _col_nzeros.end(), 0);
        std::fill(_col_nones.begin(), _col_nones.end(), 0);

        for(auto & vec : _M)
        {
            vec.resize(_size);
            vec.assign(_size, -1);
        }

        for(size_t j = 0; j < _size; ++j)
        {
            signed char val = _fmtconverter(line[j]);
            if (val == -1)
                _empty_cells.emplace(0, j);
            else
                set_value(0, j, val);
        }
        for(size_t i = 1; i < _size; ++i)
        {
            std::getline(puzzlefile, line);
            for(size_t j = 0; j < _size; ++j)
            {
                signed char val = _fmtconverter(line[j]);
                if (val == -1)
                    _empty_cells.emplace(i, j);
                else
                    set_value(i, j, val);
            }
        }
        puzzlefile.close();
    }

    inline bool is_row_filled(size_t i) const
    {
        return (_row_nzeros[i] + _row_nones[i] == _size);
    }

    inline bool is_col_filled(size_t i) const
    {
        return (_col_nzeros[i] + _col_nones[i] == _size);
    }

    inline bool is_row_balanced(size_t i) const
    {
        return (is_row_filled(i) && (_row_nones[i] == _size >> 1));
    }

    inline bool is_col_balanced(size_t i) const
    {
        return (is_col_filled(i) && (_col_nones[i] == _size >> 1));
    }

    inline size_t row_nzeros(size_t i) const
    {
        return _row_nzeros[i];
    }

    inline size_t row_nones(size_t i) const
    {
        return _row_nones[i];
    }

    inline size_t col_nzeros(size_t i) const
    {
        return _col_nzeros[i];
    }

    inline size_t col_nones(size_t i) const
    {
        return _col_nones[i];
    }

    inline bool is_row_fillable(size_t i) const
    {
        return ((_row_nzeros[i] == _size >> 1) ||
                (_row_nones[i] == _size >> 1));
    }

    inline bool is_col_fillable(size_t i) const
    {
        return ((_col_nzeros[i] == _size >> 1) ||
                (_col_nones[i] == _size >> 1));
    }

    inline bool is_valid() const
    {
        return _is_valid;
    }

    inline bool is_cell_empty(const std::pair<size_t, size_t> & p) const
    {
        return _empty_cells.count(p);
    }

    inline bool is_cell_empty(size_t x, size_t y) const
    {
        return is_cell_empty(std::make_pair(x, y));
    }

    inline size_t size() const
    {
        return _size;
    }

    inline std::vector<signed char>& operator[](std::size_t index)
    {
        return _M[index];
    }

    inline const std::vector<signed char>& operator[](std::size_t index) const
    {
        return _M[index];
    }

    inline bool has_empty_cell() const
    {
        return !_empty_cells.empty();
    }

    inline std::pair<size_t, size_t> get_empty_cell() const
    {
        auto it = _empty_cells.begin();
        return *it;
    }

    void set_value(size_t i, size_t j, signed char val)
    {
        if (val == -1)
            throw std::runtime_error("set_value() : cannot assigned -1");
        if (_M[i][j] == 0 || _M[i][j] == 1)
            throw std::runtime_error("set_value() : cell is filled");

        _M[i][j] = val;
        _inc_counters(i, j);
        _empty_cells.erase( std::pair<size_t, size_t>(i, j) );
    }

    inline void set_validity(bool validity)
    {
        _is_valid = validity;
    }
};

std::ostream& operator<<(std::ostream & os, const binary_puzzle & puzzle)
{
    for(size_t i = 0; i < puzzle.size(); ++i)
    {
        for(size_t j = 0; j < puzzle.size(); ++j)
        {
            switch(puzzle[i][j])
            {
            case 0:
                os << '0';
                break;
            case 1:
                os << '1';
                break;
            case -1:
                os << '*';
                break;
            }
        }
        if (i != puzzle.size() - 1)
            os << std::endl;
    }

    return os;
}

#endif
