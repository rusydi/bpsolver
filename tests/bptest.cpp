#define BOOST_TEST_MODULE BinaryPuzzleTest
#include <boost/test/included/unit_test.hpp>

#include"binary_puzzle.hpp"

BOOST_AUTO_TEST_SUITE(ConstructorTest)
BOOST_AUTO_TEST_CASE(EmptyConstructorTest)
{
    binary_puzzle puzzle;

    BOOST_REQUIRE_EQUAL(true, puzzle.is_valid());
    BOOST_REQUIRE_EQUAL(0, puzzle.size());
}

BOOST_AUTO_TEST_CASE(CppStrConstructorTest)
{
    const std::string path("tests/samples/6x6.txt");
    binary_puzzle puzzle(path);

    BOOST_REQUIRE_EQUAL(true, puzzle.is_valid());
    BOOST_REQUIRE_EQUAL(6, puzzle.size());
}

BOOST_AUTO_TEST_CASE(CStrConstructorTest)
{
    const char path[] = "tests/samples/8x8.txt";
    binary_puzzle puzzle(path);

    BOOST_REQUIRE_EQUAL(true, puzzle.is_valid());
    BOOST_REQUIRE_EQUAL(8, puzzle.size());
}
BOOST_AUTO_TEST_SUITE_END();

BOOST_AUTO_TEST_CASE(NonExistingFileTest)
{
    BOOST_CHECK_THROW(binary_puzzle("dummyfilename"), std::runtime_error);
}

BOOST_AUTO_TEST_CASE(ReadFileTest)
{
    binary_puzzle puzzle("tests/samples/6x6.txt");

    const std::vector<std::pair<size_t, size_t>> _filled_cells =
        { {0, 2}, {1, 0}, {1, 1}, {1, 3}, {2, 0}, {4, 3}, {5, 4} };

    BOOST_CHECK_EQUAL(1, puzzle[0][2]);
    BOOST_CHECK_EQUAL(0, puzzle[1][0]);
    BOOST_CHECK_EQUAL(0, puzzle[1][1]);
    BOOST_CHECK_EQUAL(1, puzzle[1][3]);
    BOOST_CHECK_EQUAL(0, puzzle[2][0]);
    BOOST_CHECK_EQUAL(1, puzzle[4][3]);
    BOOST_CHECK_EQUAL(0, puzzle[5][4]);

    BOOST_CHECK_EQUAL(true, puzzle.has_empty_cell());

    for(size_t i = 0; i < puzzle.size(); ++i)
    {
        for(size_t j = 0; j < puzzle.size(); ++j)
        {
            std::pair<size_t, size_t> cell = std::make_pair(i, j);

            if (std::find(_filled_cells.begin(), _filled_cells.end(),
                    cell) != _filled_cells.end())
                BOOST_CHECK_EQUAL(false, puzzle.is_cell_empty(cell));
            else
            {
                BOOST_CHECK_EQUAL(true, puzzle.is_cell_empty(cell));
                BOOST_CHECK_EQUAL(-1, puzzle[i][j]);
            }
        }
    }
}

/*
BOOST_AUTO_TEST_CASE(SetValueTest)
{
}

BOOST_AUTO_TEST_CASE(CorrectnessTest)
{
}
*/
