CXX=clang++-mp-4.0
CXXFLAGS=-std=c++11
LINKFLAGS=
OPTFLAGS=-march=native -Ofast
WARNFLAGS=-Wall

SRCDIR=src
TESTDIR=tests

DEST=$(SRCDIR)/bpsolver
TESTDEST=$(TESTDIR)/bptest

all: $(DEST).cpp
	$(CXX) $(CXXFLAGS) $(OPTFLAGS) $(WARNFLAGS) -o bpsolver $<

test: $(TESTDEST).cpp
	$(CXX) $(CXXFLAGS) $(OPTFLAGS) $(WARNFLAGS) -I $(SRCDIR)/ -o $(TESTDEST) $<
	./$(TESTDEST) --color_output=true --log_level=test_suite --output_format=HRF

clean:
	rm -f bpsolver
	rm -f $(TESTDEST)
